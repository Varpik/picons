# новости
* РБК 50
* Россия 1 +1 (Приволжье, Китеж)
* Первый канал +4 (Алтай, Дианэт)
* Россия 1 +4 (Алтай, Дианэт)
* Россия 24 +4 (Алтай, Дианэт)
* Первый канал +2 (Уфа, ЗТ)
* Россия 1 +2 (Уфа)
* Россия 24 +2 (Уфа)
* Россия 1 +0 (Липецк)
* Первый канал +0 (Невинномысск)
* Россия 1 +0 (Невинномысск)
* Россия 24 +0 (Невинномысск)
* Первый канал +7 (Владивосток)
* Россия 1 +7 (Владивосток)
* Россия 24 +7 (Владивосток)
* Россия 24 +5 (Иркутск, Орион)
* Россия 1 +5 (Братск, Орион)
* Россия 24 +5 (Братск, Орион)
* Россия 1 +4 (Абакан, Орион)
* Россия 24 +4 (Абакан, Орион)
* Первый канал +0 (Липецк)
* Россия 1 +3 (Омск)
* Россия 24 +3 (Омск)
* Первый канал +4 (Томск)
* Россия 1 +4 (Томск)
* Россия 24 +4 (Томск)
* Россия 1 +0 (Алания)
* Россия 24 +0 (Алания)
* Россия 24 +0 (Липецк)
* Первый канал +0 (Белгород)
* Россия 1 +0 (Белгород)
* Россия 24 +0 (Белгород)
* Россия 1 +4 (Иркутск, Dreamnet)
* Россия 1 +4 (Орион)
* Россия 24 +4 (Орион)
* Первый канал +3 (Омск)
* Первый канал +0 (Элиста)
* Россия 24 +2 (Пермь)
* Россия 1 +0 (Элиста)
* Россия 24 +0 (Элиста)
* Первый канал +0 (Тамбов)
* Россия 1 +0 (Тамбов)
* Россия 24 +0 (Тамбов)
* Первый канал +2 (Нефтекамск)
* Первый канал +2 (Белорецк)
* 100 % NEWS

# кино
* .sci-fi
* .red
* .black
* .red HD
* UZ Kino2 HD
* BTV Аль Пачино HD
* BTV Уилл Смит HD
* BTV Боевики CCCP
* BTV Анжелика HD
* BTV Mix Ужасы Триллеры
* BTV Фантастика VHS
* BTV Юрий Никулин
* BTV Детектив СССР
* BTV Друзья
* BTV Южный Парк / South Park HD
* BTV Восставший из ада HD
* BTV Фантастика CCCР
* BTV Пуаро Агаты Кристи HD
* BTV Дженнифер Энистон HD
* BTV Артхаус HD
* BTV По следам призраков
* BTV Одесская киностудия
* BTV Внутри Лапенко HD
* BTV Twin Peaks HD
* BTV Алексей Балабанов
* BTV Дубляж СССР
* BTV Вестерн
* BTV Карен Шахназаров
* BTV Чёртова служба в госпитале МЭШ
* Dosug TV ER
* Tv100_world_kino
* SKY HIGH ADULT HD
* YOSSO TV Thriller
* FilmZone Plus HD
* Киноман (Киносат)
* YOSSO TV Oblivion
* 33 квадратных метра
* Премьера T

# музыка
* NEW MUSIC
* MTV SE
* BTV Сектор Газа караоке
* BTV Меломан HD
* BTV Rammstein HD
* BTV Майкл Джексон
* BTV Queen
* BTV Depeche Mode
* BTV Муз-Хит за ВДВ
* BTV Классика (муз)
* BTV Каверы и лайвы HD
* BTV Dram & Bass HD
* BTV Мюзиклы
* BTV группа Ленинград
* BTV Ляпис Трубецкой
* BTV Петлюра
* SKY HIGH CLASSIC HD
* C4K360
* Муз ТВ +3 (Омск)
* Муз ТВ +0 (Элиста)

# познавательные
* Моя Стихия
* Кто куда
* Моя Стихия HD
* LIBERTY BBC
* Глазами Туриста HD

# детские
* NickToons
* WOW!TV HD
* BTV Киносказка СССР
* BTV Крот (мультсериал)
* Disney HD FR
* Рыжий (Сурдоперевод)
* Карусель +7 (Владивосток)
* Карусель +3 (Омск)
* Карусель +7
* Карусель +0 (Элиста)
* Любимое ТВ HD

# развлекательные
* UZ 6 Кадров HD
* BTV На ножах тв
* BTV Японские забавы
* BTV Уральские Пельмени HD
* BTV 6 кадров
* BTV Угадай мелодию
* BTV Женский квартал 95 HD
* BTV 95 квартал сказки HD
* СТС International
* ТНТ4 +2 (Уфа)
* ТНТ4 +0 (Тамбов)
* ОСП-студия HD
* Релакс TV

# другие
* BATICUM PLATINUM LT
* 3PLUS LT
* BALTICUM AUKSINIS LT
* RYTAS HD LT
* LTV 1 LT
* TV8 HD LIETUVA LT
* TV1000 RUSSIAN MOVIE HD LT
* NATIONAL GEOGRAPHICS HD EUROP LT
* VIASAT EXPLORE HD EUROPE LT
* MTV HITS EUROPE LT
* VIASAT NATURE HD/HISTORY HD EUROPE
* EE TV HD EE
* CHAU TV LV
* LTV 1 LV
* TV3 FILMS LV
* KIDZONE LV
* KIDZONE EE
* ETV HD EE
* TV3 ZINAS LV
* VIASAT HISTORY HD LV
* MTV HITS LV
* NTV BALTIC LV
* REN TV BALTIC LV
* FIRST CHANNEL BALTIC LV
* TV1000 ACTION LV
* OKHOTA I RYBALKA LV
* DISNEY VIASAT EU LV
* NICK JR EU LV
* VH1 EUROPE LV
* KIDZONE LT
* TV1000 EAST HD LT
* TV1000 EAST HD LV
* C MORE LIVE 1 HD SE
* C MORE LIVE 2 HD SE
* C MORE LIVE 3 HD SE
* C MORE LIVE 4 HD SE
* C MORE LIVE 5 HD SE
* C MORE SERIES HD SE
* CARTOON NETWORK SE
* DISCOVERY SCIENCE SE
* DISNEY JUNIOR SE
* DISNEY XD SE
* GODARE SE
* HORSE & COUNTRY HD SE
* ID DISCOVERY SE
* KANAL 5 HD SE
* KANAL 9 HD SE
* KUNSKAPSKANALEN HD SE
* NATIONAL GEOGRAPHICS HD SE
* NICK JUNIOR SE
* NICKELODEON SE
* NICKTOONS SE
* PARAMOUNT NETWORK SE
* SF-KANALEN SE
* SJUAN HD SE
* SVT 1 HD SE
* SVTB  SVT 24 HD SE
* TLC HD SE
* TV 3 HD SE
* TV 4 GULD SE
* TV 6 HD SE
* TV 4 FILM SE
* TV 4 FAKTA SE
* TV 4 HD SE
* VIASAT FILM HITS HD SE
* VIASAT SERIES HD SE
* VISION SE
* VIASAT FILM ACTION HD SE
* VIASAT FILM PREMIER HD SE
* VIASAT FILM FAMILY SE
* VIASAT NATURE HD SE
* VIASAT EXPLORE HD SE
* ATG LIVE SE
* C MORE FIRST HD SE
* DI TV HD SE
* EXPRESSEN TV HD SE
* HIMLEN SE
* KANAL 10 HD SE
* TV 4 NYHETERNA HD SE
* VÄSTMANLANDS TV HD SE
* ÖPPNA KANALEN SE
* HISTORY 2 HD SE
* HISTORY HD SE
* BOOMERANG SE
* BBC EARTH HD SE
* National Geographic SE
* VIASAT HISTORY HD SE
* TV8 SE
* TV10 HD SE
* VIASAT MOTOR HD SE
* Duo3 HD EE
* Duo6 HD EE
* ETV2 HD EE
* ETV HD EE
* ETV+ HD EE
* Russia today doc
* ОРТ Планета
* N24 HD DE
* SIXX HD DE
* arte HD DE
* Das Erste HD DE
* WDR HD Aachen DE
* KIKA HD DE
* ATV DE
* ORF 1 HD DE
* RTL Crime DE
* HR HD DE
* NDR HD DE
* Nickelodeon HD DE
* Cherie 25 FR
* TF1 SERIES FILMS FR
* LCI FR
* Numero 23 FR
* Science & Vie HD FR
* Rotana-Aflem AE
* Otv AE
* Mbc Max AE
* Mbc Drama AE
* Mbc Action AE
* MBC 2 SD AE
* MBC 4 AE
* National Geographic Abu Dhabi AE
* AL EMARAT TV AE
* Majid Kids TV AE
* Al Hadath AE
* Comedy Central HD ES
* Cosmopolitan HD ES
* Movistar Partidazo HD ES
* Movistar Partidazo ES
* M.LCAMPEON 1 ES
* M.LCAMPEON 2 ES
* M.LCAMPEON 3 ES
* M.LCAMPEON 5 ES
* M.LCAMPEON 8 ES
* ТВЦ International
* ТНТ International
* GOL ES
* Cuatro HD ES
* Telecinco HD ES
* DKISS ES
* tdp HD ES
* atreseries HD ES
* BeMad tv HD ES
* Realmadrid TV HD ES
* TEN ES
* mega ES
* Cinethronix RO
* DMAX HD DE
* ННТВ +0 (Китеж)
* Ростов Папа
* НТВ +4 (Алтай, Дианэт)
* 5 канал +4 (Алтай, Дианэт)
* СТС +4 (Алтай, Дианэт)
* Домашний +4 (Алтай, Дианэт)
* ТВ3 +4 (Алтай, Дианэт)
* Пятница! +4 (Алтай, Дианэт)
* Звезда +4 (Алтай, Дианэт)
* ТНТ +4 (Алтай, Дианэт)
* Губернский
* Вся Уфа
* СТС +2 (Уфа)
* ТНТ +2 (Уфа)
* НТВ +2 (Уфа)
* Звезда +2 (Уфа)
* Пятница! +2 (Уфа)
* Рен-ТВ +2 (Уфа)
* НТВ +0 (Липецк)
* 5 Канал +2 (Уфа)
* Домашний +2 (Уфа)
* 5 Канал +0 (Липецк)
* ТВ3 +2 (Уфа)
* ТВЦ +2 (Уфа)
* Домашний +0 (Невинномысск)
* НТВ +0 (Невинномысск)
* Пятница! +0 (Невинномысск)
* 5 Канал +0 (Невинномысск)
* РЕН ТВ +0 (Невинномысск)
* Россия К +0 (Невинномысск)
* СТС +0 (Невинномысск)
* ТВ3 +0 (Невинномысск)
* ТНТ +0 (Невинномысск)
* НТВ +7 (Владивосток)
* 5 канал +7 (Владивосток)
* Россия К +7 (Владивосток)
* ОТР +7 (Владивосток)
* ТВЦ +7 (Владивосток)
* РЕН ТВ +7 (Владивосток)
* СПАС +7 (Владивосток)
* Домашний +7 (Владивосток)
* ТВ3 +7 (Владивосток)
* Пятница! +7 (Владивосток)
* Звезда +7 (Владивосток)
* Мир +7 (Владивосток)
* ТНТ +7 (Владивосток)
* СТС +7 (Владивосток)
* Матур ТВ
* 26 регион HD
* АТВ Ставрополь
* СТС +5 (Иркутск, Орион)
* Домашний +5 (Иркутск, Орион)
* ТНТ +5 (Иркутск, Орион)
* БСТ +5 (Братск, Орион)
* Твой канский (Канск, Орион)
* ТНТ +4 (Канск, Орион)
* 7 канал (Абакан, Орион)
* РТС (Абакан, Орион)
* Абакан 24 (Абакан, Орион)
* БСТ +2 (Нефтекамск, ЗТ)
* БСТ +2 (Белорецк, ЗТ)
* Телеканал 360 HD +2 (Белорецк, ЗТ)
* ТВЦ +0 (Липецк)
* Пятница! +0 (Липецк)
* ТНТ +0 (Липецк)
* СТС +4 (Кузбасс)
* Домашний +3 (Омск)
* ТВ3 +3 (Омск)
* Звезда +3 (Омск)
* Мир +3 (Омск)
* ТНТ +3 (Омск)
* НТВ +2 (Нефтекамск)
* НТВ +4 (Томск)
* 5 Канал +4 (Томск)
* ТВЦ +4 (Томск)
* РЕН ТВ +4 (Томск)
* СТС +4 (Томск)
* Домашний +4 (Томск)
* ТВ3 +4 (Томск)
* Пятница! +4 (Томск)
* Звезда +4 (Томск)
* ТНТ +4 (Томск)
* Осетия Иристон
* Липецкое время
* НТВ +0 (Белгород)
* 5 Канал +0 (Белгород)
* Аист (Иркутск, DreamNet)
* СТС Прима +4 (Орион)
* Енисей +4 (Орион)
* 12 Канал +4 (Орион)
* ТВК +4 (Орион)
* Че +4 (Орион)
* 7 Канал +4 (Красноярск, Орион)
* Афоново +4 (Орион)
* 8 канал - Красноярск
* ТВ3 +0 (Белгород)
* Пятница! +0 (Белгород)
* ТНТ +0 (Белгород)
* Мир Белогорья
* Белгород 24
* НТВ +3 (Омск)
* 5 канал +3 (Омск)
* Россия К +3 (Омск)
* ТВЦ +3 (Омск)
* РЕН ТВ +3 (Омск)
* СТС +3 (Омск)
* Пятница! +3 (Омск)
* 12 Канал (Омск)
* Россия К +2 (Пермь)
* НТВ +0 (Элиста)
* 5 Канал +0 (Элиста)
* Россия К +0 (Элиста)
* ТВЦ +0 (Элиста)
* РЕН ТВ +0 (Элиста)
* СТС +0 (Элиста)
* ТВ3 +0 (Элиста)
* Звезда +0 (Элиста)
* Мир +0 (Элиста)
* ТНТ +0 (Элиста)
* ТНТ +2 (Белорецк)
* Первый городской (Омск)
* НТВ +0 (Тамбов)
* 5 канал +0 (Тамбов)
* РЕН ТВ +0 (Тамбов)
* СТС +0 (Тамбов)
* Домашний +0 (Тамбов)
* Пятница! +0 (Тамбов)
* ТНТ +0 (Тамбов)
* Продвижение (Тамбов)
* ОТВ (ЗТ Владивосток)
* Домашний +0 (Белгород)
* Продвижение (Липецк)
* СТС +0 (Белгород)
* РЕН ТВ +0 (Белгород)
* СТС +2 (Нефтекамск)
* Пятница! +2 (Нефтекамск)
* ТВ3 +2 (Нефтекамск)
* ТНТ +2 (Нефтекамск)
* Нефтекамск 24
* Ника ТВ
* Новый Мир
* Небеса ТВ7 HD
* Истоки Орёл
* 12 канал ОМСК HD
* Ноябрьск 24 HD
* Мособр TV HD
* #ё samara HD

# спорт
* Discovery Channel US
* Canal+ Sport 2 HD PL
* EUROSPORT 1 HD UK
* Sky Sports Action HD UK
* V Sport Football FHD
* V Sport Hockey FHD
* VIASAT SPORT URHEILU HD
* VIASAT SPORT JALKAPALLO HD
* C MORE FOOTBALL HD SE
* C MORE GOLF HD SE
* C MORE HITS HD SE
* C MORE HOKEY HD SE
* C MORE STARTS HD SE
* EUROSPORT 1 HD SE
* EUROSPORT 2 HD SE
* VIASAT SPORT EXTRA HD SE
* VIASAT SPORT HD SE
* VIASAT SPORT FOOTBAL HD SE
* VIASAT SPORT GOLF HD SE
* VIASAT SPORT HOKEY HD SE
* VIASAT SPORT PREMIUM HD SE
* TV3 SPORT SE
* UK Premier Sports 1 SD IE
* EuroSport 360 8 HD FR
* Живи Активно HD
* Eurosport_Gold_HD
* Eurosport HD DE
* Setanta Eurasia HD
* Setanta Eurasia+
* Fastnfunbox
* Спорт 1 HD UA
* МАТЧ! +4 (Алтай, Дианэт)
* МАТЧ! +2 (Уфа)
* МАТЧ! +0 (Липецк)
* МАТЧ! +7 (Владивосток)
* МАТЧ! +4 (Томск)
* МАТЧ! +0 (Белгород)
* МАТЧ! +3 (Омск)
* МАТЧ! +0 (Элиста)

# HD
* ZMaxx HD DE
* Dazn F1 HD ES
* beIN Sports 2 Premium HD QA
* beIN Sports 1 Premium HD QA
* beIN Sports 1 HD EN
* beIN Sports 2 HD EN
* Матч Игра HD 50
* МАТЧ! Футбол 1 HD 50
* Матч Футбол 3 HD 50
* МАТЧ! HD 50
* МАТЧ Премьер HD 50
* National Geographic Channel HD 50
* Еда Premium 50
* Food network HD 50
* Матч футбол 2 HD 50

# взрослые
* O-La-La orig
* BRAZZERS TV
* Bang U
* PASSION XXX
* X Plus Milf HD
* ROCCO TV 7/24
* BTV Японские sex game

# Հայկական
* H2 AM
* Arm News HD AM
* Ararat AM
* Erkir AM
* H3 AM
* Dar 21 AM
* Ar AM
* Shoghakat AM
* Luys AM
* Հինգերորդ ալիք Plus
* Նուռ  TV
* Arm News
* Լուրեր
* USA Armenia
* Pan Armenian TV
* Horizon
* Kotayk TV
* Ցայգ TV
* Լոռի TV
* FreeNews
* Armount TV
* Ֆորտունա TV
* Arm Toon TV
* Դելտա TV
* LiveNews.am
* Vmedia
* Vmedia +
* Pan Armenian TV AM
* Artn AM
* Horizon TV AM
* TBN Armenia AM
* LiveNews TV AM

# українські
* UA:Тернопіль
* Megogo live HD UA
* НАШ UA
* Терра UA
* XSPORT Plus HD UA
* Quiz TV UA
* Kino 1 UA
* Film Box Art House HD UA
* Film Box UA
* ТЕЛЕВСЕСВІТ UA
* КАРАВАН-ТВ UA
* Донеччина.TV UA
* Наше РЕТРО UA
* ПЕРШИЙ ЗАХІДНИЙ HD UA
* ПРАВДАТУТ HD UA
* ПЕРШИЙ ДІЛОВИЙ UA
* ЕКО–ТВ UA
* EU MUSIC UA

# moldovenească
* TeenNick RO

# türk
* beIN Sports 1 HD TR
* Turk Karadeniz TR
* Tatlises
* TRT HD 4K
* Kanal V TR
* Kocaeli TV TR
* Ege TV TR
* KRT TR
* Rumeli TV TR
* Kanal Cay TR
* T.A.Y TV TR

# ישראלי
* Yes Movies Comedy IL
* DISNEY JR IL
* EGO TOTAL HD IL
* FOOD CHANNE IL
* HEALTH CHANNEL HD IL
* HIDABRUT IL
* HOME PLUS HD IL
* HOP CHILDHOOD IL
* HOT3 HD IL
* HOT CINEMA 2 HD IL
* HOT CINEMA 4 HD IL
* HOT CINEMA 3 HD IL
* HOT HBO HD IL
* HOT ZONE HD IL
* LIFETIME HD IL
* NICK JR HD IL
* NICK HD IL
* ONE 1 HD IL
* VIVA HD IL
* KAN 11 IL
* CHANNEL 9 HD IL
* BABY TV IL
* JUNIOR IL
* DISCOVERY CHANNEL HD IL
* DISNEY CHANNEL HD IL
* HOP HD IL
* HOT CINEMA 1 HD IL
* LULI IL
* NATIONAL GEOGRAPHICS HD IL
* NET GEO_WILD HD IL
* SPORT 1 HD IL
* SPORT 3 HD IL
* SPORT 4 HD IL
* SPORT 5 GOLD IL
* SPORT 5 LIVE HD IL
* YES DOCU HD IL
* NAT GEO WILD IL
* CHANNEL 20 HD IL
* GOOD LIFE TV IL
* HALA TV IL
* A+ HD IL
* KNESET CHANNE IL
* YES MOVIES ACTION HD IL
* YES MOVIES DRAMA HD IL
* YES MOVIES KIDS HD IL
* MTV MUSIC IL
* TRAVEL CHANNEL IL
* YES TV ACTION HD IL
* YES TV COMEDY HD IL
* YES TV DRAMA HD IL

# HD Orig
* BBC Earth FHD PL
* .red HD orig
* .sci-fi orig
* .black orig
* .red orig
* Моя Стихия orig
* Восьмой канал orig
* Столичный Магазин Orig
* Наше Крутое HD orig

# 4K
* BTV Relax 4k
* Наша сибирь 4K [HEVEC]
* Home 4K [HEVEC]
* NASA TV 4K
